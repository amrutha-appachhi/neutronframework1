package tests.pages;

import java.util.Map;

import org.testng.Assert;

import agent.IAgent;
import agent.internal.MobileAgent;
import central.Configuration;
import enums.ConfigType;

public class LoginPage extends FullPage {

	public LoginPage(Configuration conf, IAgent agent, Map<String, String> testData) throws Exception {
		super(conf, agent, testData);
		assertPageLoad();
	}

	public void login() throws Exception {
		logger.debug(String.format("(%s) Logging in...", this.getPlatform()));
		switch (this.getPlatform()) {
		case IOS:
			break;
		case IOS_WEB:
			break;
		case ANDROID:
			takeSnapShot();
			enterUserNamePassword(this.getTestData());
			takeSnapShot();
			getControl("btnLogin").click();
			Thread.sleep(10000);
			takeSnapShot();
			getControl("btnAcceptAndLogin").click();
			Thread.sleep(10000);
			takeSnapShot();
			break;
		case ANDROID_WEB:
			break;
		case DESKTOP_WEB:
		}
	}
	
	private void enterUserNamePassword(Map<String, String> testData) throws Exception {
		getControl("txtUsername").enterText(testData.get("username"));
		getControl("txtPassword").enterText(testData.get("password"));
	}
}
