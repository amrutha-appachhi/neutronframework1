package tests;

import org.testng.annotations.Test;

public class LoginFlow extends SupportTest {

	@Test
	public void Login() throws Exception {
		logger.debug(this.getTestStartInfoMessage("Prospect Customer"));
		login.login();
		logger.debug(this.getTestEndInfoMessage("Prospect Customer"));	
	}
}