package tests.pages;

import java.util.Map;

import org.testng.Assert;

import agent.IAgent;
import agent.internal.MobileAgent;
import central.Configuration;
import enums.ConfigType;

public class ProspectPage extends FullPage {

	public ProspectPage(Configuration conf, IAgent agent, Map<String, String> testData) throws Exception {
		super(conf, agent, testData);
		assertPageLoad();
	}

	public void searchProspectCustomer() throws Exception {
		logger.debug(String.format("(%s) Logging in...", this.getPlatform()));
		switch (this.getPlatform()) {
		case IOS:
			break;
		case IOS_WEB:
			break;
		case ANDROID:
			getControl("prospectCardButton").click();
			searchPC(this.getTestData());
			getControl("searchPCButton").click();
			Thread.sleep(20000);
			takeSnapShot();
			break;
		case ANDROID_WEB:
			break;
		case DESKTOP_WEB:
		}
	

	}
	
	private void searchPC(Map<String, String> testData) throws Exception {
		getControl("txtsearchPC").enterText(testData.get("mobileNumber"));	
	}



	public void assertPageLoad() throws Exception {
		super.assertPageLoad();
//		switch (this.getPlatform()) {
//		case ANDROID:
//			//Thread.sleep(10000);
//			Assert.assertTrue(getControl("skipBtn").isVisible(), "Loginpage not displayed");
//
//			break;
//
//		}
	}
}
