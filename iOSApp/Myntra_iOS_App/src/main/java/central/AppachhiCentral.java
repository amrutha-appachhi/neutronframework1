package central;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.ITestContext;

import enums.ConfigType;
import pagedef.PageDef;
import utils.IniReader;

public enum AppachhiCentral {
	INSTANCE;

	private static Logger logger = null;
	private Configuration centralConf = null;
	private Map<String, Configuration> contextConfigs = new HashMap<String, Configuration>();
	private Map<String, IniReader> contextTestData = new HashMap<String, IniReader>();
	private ObjectRepository pageDefs = null;
	private String screenDateFormat = null;

	public void init() throws Exception {
		String log4jConfigFile = getPath("conf/log4j.properties");
		PropertyConfigurator.configure(log4jConfigFile);
		logger = Logger.getRootLogger();
		loadCentralConf();
		loadObjectRepository();
	}

	private String getPath(String relPath) throws Exception {
		return (new File(AppachhiCentral.class.getClassLoader().getResource(relPath).toURI())).getAbsolutePath();
	}

	public String getDirPathForConf(ConfigType confConst) throws Exception {
		return getPath(centralConf.getValue(confConst));
	}

	public String getScreenShotsDir() {
//		return System.getProperty("user.dir") + File.separator + centralConf.getValue(ConfigType.SCREENSHOTS_DIR);
		logger.info(String.format("Screenshotpath 	 "
				+ " %s ", System.getProperty("screenshot_path").toString()));
		return System.getProperty("screenshot_path");
	}

	private void loadCentralConf() throws Exception {
		if (centralConf != null)
			return;
		String confPath = getPath("conf/config.ini");
		IniReader reader = new IniReader(confPath);
		this.centralConf = new Configuration(reader.getSectionData("Central"));
		this.screenDateFormat = centralConf.getValue(ConfigType.SCRREENSHOT_TSTAMP);
	}

	public String getScreenShotTimeStampFormat() {
		return this.screenDateFormat;
	}

	private void loadObjectRepository() throws Exception {
		pageDefs = new ObjectRepository(getDirPathForConf(ConfigType.PAGEDEF_DIR));
	}

	public synchronized void registerContext(ITestContext context) throws Exception {
		Configuration contextConf = new Configuration(centralConf, context);
		this.contextConfigs.put(context.getName().toLowerCase(), contextConf);
		String testDataFilePath = String.format("%s/%s/testdata.ini", getDirPathForConf(ConfigType.DATA_DIR),
				System.getProperty("platform"));
		logger.debug(String.format("Loading Test Data File at Path :: %s", testDataFilePath));
		IniReader reader = new IniReader(testDataFilePath);
		this.contextTestData.put(context.getName().toLowerCase(), reader);
	}

	public synchronized Configuration getContextConfig(ITestContext context) {
		return this.contextConfigs.get(context.getName().toLowerCase());
	}

	public synchronized Map<String, String> getTestData(ITestContext context, String section) {
		return this.contextTestData.get(context.getName().toLowerCase()).getSectionData(section);
	}

	public static Logger getLogger() {
		return logger;
	}

	public synchronized PageDef getPageDef(String pageName) {
		return pageDefs.getPageDef(pageName);
	}

}