package agent.internal;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import agent.IAgent;
import central.Configuration;
import enums.ConfigType;
import enums.DesktopBrowser;
import enums.Platform;

public class WebAgentFactory {
	static WebDriver driver = null;

	public static IAgent createAgent(Configuration config) throws Exception {
		Platform platform = config.getPlatform();
		DesktopBrowser browser = DesktopBrowser.valueOf(System.getProperty("browser").toUpperCase());
		switch (platform) {
		case DESKTOP_WEB:
			initDriver(config, browser);
			driver.get(System.getProperty("app_browser_url"));
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			break;
		default:
			throw new Exception("Invalid platform, Supported platform is DESKTOP_WEB");
		}

		return new DesktopWebAgent(config, driver);
	}

	private static WebDriver initDriver(Configuration config, DesktopBrowser browser) throws Exception {
		DesiredCapabilities caps = null;
		switch (browser) {
		case CHROME:
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			options.addArguments("--disable-infobars");
			caps = DesiredCapabilities.chrome();
			caps.setCapability(ChromeOptions.CAPABILITY, options);
			caps.setCapability("pageLoadStrategy", "none");
			caps.setVersion(ConfigType.PLATFORM_VER.toString());
			setPropertyByOS(browser);
			driver = new ChromeDriver(caps);

			break;
		case EDGE:
			caps = DesiredCapabilities.internetExplorer();
			caps.setVersion(ConfigType.PLATFORM_VER.toString());
			setPropertyByOS(browser);
			driver = new EdgeDriver(caps);
			break;
		case FIREFOX:
			caps = DesiredCapabilities.firefox();
			caps.setVersion(ConfigType.PLATFORM_VER.toString());
			FirefoxProfile Profile = new FirefoxProfile();
			Profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/xml");
			driver = new FirefoxDriver(caps);
			break;
		case IE:
			caps = DesiredCapabilities.internetExplorer();
			caps.setVersion(ConfigType.PLATFORM_VER.toString());
			caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			driver = new InternetExplorerDriver(caps);
			break;
		case SAFARI:
			caps = DesiredCapabilities.safari();
			caps.setVersion(ConfigType.PLATFORM_VER.toString());
			driver = new SafariDriver(caps);
			break;
		default:
			break;
		}
		return driver;
	}

	private static void setPropertyByOS(DesktopBrowser browser) throws Exception {
		String driverPath = System.getProperty("user.dir") + "/src/test/resources/drivers/desktop_web/";
		switch (getOS()) {
		case "OS_WINDOWS":
			switch (browser) {
			case CHROME:
				System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
				break;
			case EDGE:
				System.setProperty("webdriver.edge.driver", driverPath + "MicrosoftWebDriver.exe");
				break;
			default:
				break;
			}
			break;
		case "OS_MAC":
			switch (browser) {
			case CHROME:
				System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver");
				break;
			default:
				break;
			}
			break;
		default:
			throw new Exception("Unknown OS, Please check the Operating System Paramter");
		}
	}

	public static String getOS() {
		String osType = null;
		String osName = System.getProperty("os.name");
		String osNameMatch = osName.toLowerCase();
		if (osNameMatch.contains("linux")) {
			osType = "OS_LINUX";
		} else if (osNameMatch.contains("windows")) {
			osType = "OS_WINDOWS";
		} else if (osNameMatch.contains("solaris") || osNameMatch.contains("sunos")) {
			osType = "OS_SOLARIS";
		} else if (osNameMatch.contains("mac os") || osNameMatch.contains("macos") || osNameMatch.contains("darwin")) {
			osType = "OS_MAC";
		} else {
			osType = "Unknown";
		}
		return osType;
	}
}
