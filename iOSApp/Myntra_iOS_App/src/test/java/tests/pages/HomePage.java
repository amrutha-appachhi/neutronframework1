package tests.pages;

import java.util.Map;

import org.openqa.selenium.Keys;

import agent.IAgent;
import central.Configuration;
import enums.Platform;

public class HomePage extends FullPage {
	public HomePage(Configuration conf, IAgent agent, Map<String, String> testData) throws Exception {
		super(conf, agent, testData);
		assertPageLoad();
	}

	public HomePage searchItem() throws Exception {
		String searchItem = getTestData().get("SearchItem");
		switch (this.getPlatform()) {
		case IOS:
			takeSnapShot();
			getControl("icoHome").click();
			takeSnapShot();
			getControl("btnSearch").click();
			takeSnapShot();
			getControl("txtSearchField").enterText(searchItem);
			takeSnapShot();
			getControl("txtSearchField").enterText(Keys.ENTER);
			takeSnapShot();
			takeSnapShot();
			break;
		case IOS_WEB:
			break;
		case ANDROID:
			getControl("btnSearch").click();
			getControl("txtSearchField").enterText(searchItem + "\\n");
			break;
		case ANDROID_WEB:
			getControl("btnSearch").click();
			getControl("txtSearchField").enterText(searchItem);
			getControl("txtSearchField").enterText(Keys.ENTER);
			break;
		case DESKTOP_WEB:
			getControl("txtSearchField").enterText(searchItem);
			getControl("btnSearch").click();
		}

		return this;

	}


	protected void logout() throws Exception {
		if (this.getPlatform() == Platform.IOS) {
			getControl("icoProfile").click();
			try {
				getControl("btnLogOut").click();
			} catch (Exception e) {
				// User is not logged in. Ignore.
			}
		}
	}

	public void assertPageLoad() throws Exception {
		super.assertPageLoad();
	}
}
