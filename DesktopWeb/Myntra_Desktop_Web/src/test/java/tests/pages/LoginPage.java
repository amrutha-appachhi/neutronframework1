package tests.pages;

import java.io.File;
import java.util.Map;

import org.testng.Assert;

import agent.IAgent;
import central.Configuration;
import enums.ConfigType;

public class LoginPage extends FullPage {

	public LoginPage(Configuration conf, IAgent agent, Map<String, String> testData) throws Exception {
		super(conf, agent, testData);
		assertPageLoad();
	}

	public HomePage signup() throws Exception {
		logger.debug(String.format("(%s) Logging in...", this.getPlatform()));
		HomePage home = null;
		switch (this.getPlatform()) {
		case IOS:
			break;
		case IOS_WEB:
			break;
		case ANDROID:
			break;
		case ANDROID_WEB:
			break;
		case DESKTOP_WEB:
			enterUserNamePassword(this.getTestData());
			getControl("dropApk").uploadFile(System.getProperty("user.dir")+"/apps/LinkedinLite_1.7.5.apk" );
			getControl("btnSignup").click();
			Thread.sleep(15000);
			break;
		}
		return new HomePage(this.getConfig(), this.getAgent(), this.getTestData());

	}

	public HomePage login() throws Exception {
		logger.debug(String.format("(%s) Logging in...", this.getPlatform()));
		HomePage home = null;
		switch (this.getPlatform()) {
		case IOS:
			break;
		case IOS_WEB:
			break;
		case ANDROID:
			break;
		case ANDROID_WEB:
			break;
		case DESKTOP_WEB:
			getControl("tabLogin").click();
			Thread.sleep(5000);
			enterEmailIDPassword(this.getTestData());
			getControl("btnLogin").click();
			Thread.sleep(25000);
			break;
		}
		return new HomePage(this.getConfig(), this.getAgent(), this.getTestData());

	}


	
	private void enterEmailIDPassword(Map<String, String> testData) throws Exception {
		getControl("txtEmailLogin").enterText(testData.get("emailid"));
		getControl("txtPasswordLogin").enterText(testData.get("password"));
	}

	private void enterUserNamePassword(Map<String, String> testdata) throws Exception {
		getControl("txtEmail").enterText("test"+generateString(5)+testdata.get("username"));
		getControl("txtPassword").enterText(testdata.get("password"));
		getControl("txtPhone").enterText("85"+generateRandomNumber(8));
	}
	
	
	public void assertPageLoad() throws Exception {
		super.assertPageLoad();
//		switch (this.getPlatform()) {
//		case ANDROID:
//			//Thread.sleep(10000);
//			Assert.assertTrue(getControl("skipBtn").isVisible(), "Loginpage not displayed");
//
//			break;
//
//		}
	}
}
