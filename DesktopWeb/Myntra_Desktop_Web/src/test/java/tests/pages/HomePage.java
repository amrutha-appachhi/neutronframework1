package tests.pages;

import static org.testng.Assert.assertTrue;

import java.util.Map;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import agent.IAgent;
import central.Configuration;
import enums.Platform;
import io.appium.java_client.android.AndroidElement;

public class HomePage extends FullPage {
    public HomePage(Configuration conf, IAgent agent, Map<String, String> testData) throws Exception {
        super(conf, agent, testData);
    }
    
    public LoginPage viewPlaybackLogs() throws Exception {
        switch (this.getPlatform()) {
        case IOS:
            break;
        case IOS_WEB:
            break;
        case ANDROID:
            getControl("txtFeed").click();
            swipeDown();
            break;
        case ANDROID_WEB:
            break;
        case DESKTOP_WEB:
            if(getControl("noReports").isVisible()) {
                refreshPage();
            }
            Thread.sleep(15000);
            if(getControl("playbackLogs").isEnabled()) {
                getControl("playbackLogs").click();
            }
            getControl("btnLogout").click();
            Thread.sleep(25000);
            break;
        }
        return new LoginPage(this.getConfig(), this.getAgent(), this.getTestData());
    }
    
    public LoginPage viewReport() throws Exception {
        switch (this.getPlatform()) {
        case IOS:
            break;
        case IOS_WEB:
            break;
        case ANDROID:
            break;
        case ANDROID_WEB:
            break;
        case DESKTOP_WEB:
            getControl("txtAndroid").click();
            if(getControl("noReports").isVisible()) {
                refreshPage();
            }
            Thread.sleep(15000);
        
//            if(getControl("playbackLogs").isEnabled()) {
//                getControl("playbackLogs").click();
//            }
//            Thread.sleep(10000);
////            getControl("btnLogout").click();
//            Thread.sleep(25000);
            takeSnapShot();
            takeSnapShot();
            break;
        }
        return new LoginPage(this.getConfig(), this.getAgent(), this.getTestData());
    }
    
    public void refreshPage() throws Exception {
        getControl("txtAndroid").enterText(Keys.F5);
    }
    
    public void assertPageLoad() throws Exception {
        super.assertPageLoad();
        switch (this.getPlatform()) {
        case ANDROID:
            assertTrue(getControl("txtTrack").isVisible(), "Reason:Login is not successful|Expected:Login to be successful");
            break;
            
        }
    }
    
}