package tests.pages;

import java.util.Map;
import agent.IAgent;
import central.Configuration;
import enums.ConfigType;

public class LoginPage extends FullPage {
	
	public LoginPage(Configuration conf, IAgent agent, Map<String, String> testData) throws Exception {
		super(conf, agent, testData);
		assertPageLoad();
	}
	public void login() throws Exception {
		logger.debug(String.format("(%s) Logging in...", this.getPlatform()));

		switch (this.getPlatform()) {
		case IOS:
			break;
		case IOS_WEB:
			break;
		case ANDROID:
			break;
		case ANDROID_WEB:
			this.getAgent().goTo(System.getProperty("app_browser_url"));
			Thread.sleep(10000);
			getControl("lnkMenu").click();
			Thread.sleep(10000);
			getControl("lnkLogIn").click();
			Thread.sleep(5000);
			enterUserNamePassword(this.getTestData());
			Thread.sleep(10000);
			takeSnapShot();
			break;
		case DESKTOP_WEB:
		}
	}

	private void enterUserNamePassword(Map<String, String> testData) throws Exception {
		getControl("txtEmailId").enterText(testData.get("UserName"));
		takeSnapShot();
		getControl("btnContinue").click();
		Thread.sleep(5000);
		getControl("txtPassword").enterText(testData.get("Password"));
		takeSnapShot();
		getControl("btnLogin").click();
	}

	public void handlePermissions() throws Exception {
		handlePermitOverDraw();
		try {
			getControl("btnPermissionLater").click();
		} catch (Exception e) {
			// Pop-up not displayed. Ignore.
		}
	}

	private void handlePermitOverDraw() {
		try {
			getControl("rdoPermitOverDraw").click();
			this.getAgent().goBack();
		} catch (Exception e) {
			// Permit overdraw screen did not appear. Ignore.
		}
	}

	public void assertPageLoad() throws Exception {
		super.assertPageLoad();
		switch (this.getPlatform()) {
		case ANDROID:
			getControl("btnOBSLogin").assertVisible();
			break;

		}
	}
}
