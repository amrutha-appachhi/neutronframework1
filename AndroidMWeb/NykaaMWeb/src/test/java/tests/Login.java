package tests;

import org.testng.annotations.Test;

public class Login extends SupportTest {

	@Test(groups = { "Login" }, description = "")
	public void Login() throws Exception {
		logger.debug(this.getTestStartInfoMessage("Login"));
		login.login();
		logger.debug(this.getTestEndInfoMessage("Login"));
	}
}