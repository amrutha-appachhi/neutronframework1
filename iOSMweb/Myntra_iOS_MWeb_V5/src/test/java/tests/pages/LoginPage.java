package tests.pages;

import java.util.Map;

import agent.IAgent;
import central.Configuration;
import enums.ConfigType;
import tests.pages.HomePage;

public class LoginPage extends FullPage {

	public LoginPage(Configuration conf, IAgent agent, Map<String, String> testData) throws Exception {
		super(conf, agent, testData);
		assertPageLoad();
	}

	public HomePage login() throws Exception {
		logger.debug(String.format("(%s) Logging in...", this.getPlatform()));
		HomePage home = null;
		switch (this.getPlatform()) {
		case IOS:
			home = new HomePage(this.getConfig(), this.getAgent(), this.getTestData());
			home.logout();
			getControl("mnuHamburger").click();
			getControl("lnkLogIn").click();
			enterUserNamePassword(this.getTestData());
			break;
		case IOS_WEB:
			this.getAgent().goTo(System.getProperty("app_browser_url"));
			takeSnapShot();
			home = new HomePage(this.getConfig(), this.getAgent(), this.getTestData());
			home.logout();
			Thread.sleep(5000);			
			try {
				getControl("mnuHamburger").click();
				getControl("lnkLogIn").click();
			} catch (Exception e) {
				
			}
			enterUserNamePassword(this.getTestData());
			break;
		case ANDROID:
			break;
		case ANDROID_WEB:
			break;
		case DESKTOP_WEB:
			break;
		}
		return new HomePage(this.getConfig(), this.getAgent(), this.getTestData());

	}

	private void enterUserNamePassword(Map<String, String> testData) throws Exception {
		getControl("txtEmailId").enterText(testData.get("UserName"));
		getControl("txtPassword").enterText(testData.get("Password"));
		getControl("btnLogin").click();

	}

	public void assertPageLoad() throws Exception {
		super.assertPageLoad();
		}
}
