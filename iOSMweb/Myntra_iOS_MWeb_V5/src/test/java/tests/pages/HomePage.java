package tests.pages;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import agent.IAgent;
import central.Configuration;
import enums.Platform;

public class HomePage extends FullPage {
	public HomePage(Configuration conf, IAgent agent, Map<String, String> testData) throws Exception {
		super(conf, agent, testData);
		assertPageLoad();
	}

	public HomePage searchItem() throws Exception {
		String searchItem = getTestData().get("SearchItem");
		switch (this.getPlatform()) {
		case IOS:
			getControl("icoHome").click();
			getControl("btnSearch").click();
			getControl("txtSearchField").enterText(searchItem);
			getControl("txtSearchField").enterText(Keys.ENTER);
			break;
		case IOS_WEB:
			Thread.sleep(5000);
			// getControl("icoHome").click();
			getControl("iconSearch").click();
			getControl("txtSearchField").enterText(searchItem);
			getControl("txtSearchField").enterText(Keys.ENTER);
//			getControl("btnSearch").click();
			break;
		case ANDROID:
			break;
		case ANDROID_WEB:
			break;
		case DESKTOP_WEB:
			break;
		}

		return this;

	}


	protected void logout() throws Exception {
		if (this.getPlatform() == Platform.IOS) {
			getControl("icoProfile").click();
			try {
				getControl("btnLogOut").click();
			} catch (Exception e) {
				// User is not logged in. Ignore.
			}
		}
		if (this.getPlatform() == Platform.IOS_WEB) {
			getControl("mnuHamburger").click();
			getControl("lnkLogIn").click();
			try {
			getControl("btnLogOut").hover();
			Thread.sleep(3000);
			getControl("btnLogOut").click();
			} catch (Exception e) {
				// User is not logged in. Ignore.
			}
	}
}
	public void assertPageLoad() throws Exception {
		super.assertPageLoad();
	}


}
